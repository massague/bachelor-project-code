from attacks_utils_ltv import AdversarialPatchPyTorchModified
from attacks_utils import plots, load_model
from caltech_utils import load_caltech
from evaluation_metrics import evaluate_patch
import pickle

#  Attack settings
rotation_max = 10
scale_min = 0.4
scale_max = 1.0
learning_rate = 1
batch_size = 15
max_iter = 40
patch_shape = (3, 100, 100)
optimizer = 'Adam'

def save_ap_state(ap, filename):
    state = {
        'patch': ap._patch,
        'patch_grad': ap._patch.grad,  # Save the gradients
        'loss_history': ap.loss_history,
        'grad_magnitude_history': ap.grad_magnitude_history,
        'max_iter': ap.max_iter,
        'optimizer_state_dict': ap._optimizer.state_dict(),
    }
    with open(filename, 'wb') as file:
        pickle.dump(state, file)

def load_ap_state(filename, detector):
    with open(filename, 'rb') as file:
        state = pickle.load(file)
    ap_loaded = AdversarialPatchPyTorchModified(
        estimator=detector,
        rotation_max=rotation_max,
        scale_min=scale_min,
        scale_max=scale_max,
        learning_rate=learning_rate,
        batch_size=batch_size,
        max_iter=state['max_iter'],
        patch_shape=patch_shape,
        patch_type='square',
        verbose=True,
        optimizer=optimizer)
    ap_loaded._patch = state['patch']
    ap_loaded._patch.grad = state['patch_grad']  # Load the gradients
    ap_loaded.loss_history = state['loss_history']
    ap_loaded.grad_magnitude_history = state['grad_magnitude_history']
    ap_loaded._optimizer.load_state_dict(state['optimizer_state_dict'])
    return ap_loaded

def main():
    print("Predicted annotations caltech ltv")
    detector = load_model()
    x, _ = load_caltech("caltech_converted_data", subset="train", num_samples_per_set=215)

    target = detector.predict(x)
    print(f'TOTAL NUMBER OF IMAGES: ', len(x))
    total_batches = len(x) // batch_size + (len(x) % batch_size != 0)
    print("Total batches: ", total_batches)
    ap = AdversarialPatchPyTorchModified(
        estimator=detector,
        rotation_max=rotation_max,
        scale_min=scale_min,
        scale_max=scale_max,
        learning_rate=learning_rate,
        batch_size=batch_size,
        max_iter=max_iter,
        patch_shape=patch_shape,
        patch_type='square',
        verbose=True,
        optimizer=optimizer,
        total_batches=total_batches)

    ap.generate(x=x, y=target)
    print("Done generating the patch!")
    plots(ap, x, detector, name="40iter_predicted_caltech_ltv")
    print("Done generating the plots!")
    # Save the state of the AdversarialPatchPyTorchModified object
    save_ap_state(ap, '/home/massague/RESULTS_inria/ap_state_40iter_predicted_caltech_ltv.pkl')
    print("Done saving the state, starting evaluation!")
    evaluate_patch(ap, detector, "caltech")
    evaluate_patch(ap, detector=detector, dataset="inria")
    print("Done evaluating the patch!")
if __name__ == "__main__":
    main()