import numpy as np
import torch
from typing import Optional
from art.estimators.object_detection.pytorch_yolo import PyTorchYolo
from art.attacks.evasion import AdversarialPatchPyTorch
import yolov5
from yolov5.utils.loss import ComputeLoss
import cv2
import matplotlib.pyplot as plt
class AdversarialPatchPyTorchModified(AdversarialPatchPyTorch): #so we can monitor the evolution of the loss
    def __init__(self, *args,  **kwargs):
        self.total_batches = kwargs.pop('total_batches', None) # Extract total_batches from kwargs
        super().__init__(*args, **kwargs)
        self.loss_history = []
        self.grad_magnitude_history = []
        self.iter_average_loss_history = []
        self.iter_average_grad_magnitude_history = []
        self.batch_couter = 0

    def _train_step(
            self, images: "torch.Tensor", target: "torch.Tensor", mask: Optional["torch.Tensor"] = None
        ) -> "torch.Tensor":
        images = images.to(self.estimator.device) 
        self.estimator.model.zero_grad()
        loss = self._loss(images, target, mask)
        loss.backward(retain_graph=True)
        

        if self._optimizer_string == "pgd":
            if self._patch.grad is not None:
                gradients = self._patch.grad.sign() * self.learning_rate
            else:
                raise ValueError("Gradient term in PyTorch model is `None`.")

            with torch.no_grad():
                self._patch[:] = torch.clamp(
                    self._patch + gradients, min=self.estimator.clip_values[0], max=self.estimator.clip_values[1]
                )
        else:
            self._optimizer.step()

            with torch.no_grad():
                self._patch[:] = torch.clamp(
                    self._patch, min=self.estimator.clip_values[0], max=self.estimator.clip_values[1]
                )

        self.loss_history.append(loss.item()) #store loss value
        grad_magnitude = torch.norm(self._patch.grad).item()
        self.grad_magnitude_history.append(grad_magnitude)
        self.batch_couter += 1
        
        # Reset variables after all batches for an iteration are processed
        if self.batch_couter == self.total_batches:
            avg_loss = sum(self.loss_history)/self.batch_couter
            avg_grad_magnitude = sum(self.grad_magnitude_history)/self.batch_couter
            self.iter_average_loss_history.append(avg_loss)
            self.iter_average_grad_magnitude_history.append(avg_grad_magnitude)
            self.loss_history = []
            self.grad_magnitude_history = []
            self.batch_couter = 0


        return loss
    
    def _non_printability_score(self, patch: "torch.Tensor") -> "torch.Tensor":
        #ASSUMING PATCH COLOURS ARE IN THE RANGE [0,1]
        # Define the printable_colors  here
        printable_colors_list = [
            (255, 0, 0),    # Red
            (0, 255, 0),    # Green
            (0, 0, 255),    # Blue
            (255, 255, 0),  # Yellow
            (255, 0, 255),  # Magenta
            (0, 255, 255),  # Cyan
            (0, 0, 0),      # Black
            (255, 255, 255) # White
        ]
        printable_colors_np = np.array(printable_colors_list, dtype=np.float32) / 255.0
        printable_colors = torch.tensor(printable_colors_np, dtype=torch.float32).unsqueeze(0)
        patch_expanded = patch.view(-1, 3).unsqueeze(1) # Expand the patch tensor to shape (num_pixels, 1, 3)
        color_diffs = torch.abs(patch_expanded - printable_colors) # Compute the absolute difference between the patch and the printable colors
        min_color_diffs = torch.min(color_diffs, dim=2) # Compute the minimum color difference for each pixel
        lnps = torch.sum(min_color_diffs)
        return lnps
    

    def _total_variation_loss(self, patch: "torch.Tensor") -> "torch.Tensor":
        """
        Compute the total variation loss for a given patch.
        
        Args:
            patch: A tensor representing the patch whose total variation loss we want to compute.

        Returns:
            The total variation loss of the patch.
        """

        patch_norm = patch / 255.0  
        """
        ltv = torch.sum(
            torch.sqrt(
                (patch_norm[:, :-1, :-1] - patch_norm[:, 1:, :-1]) ** 2
                + (patch_norm[:, :-1, :-1] - patch_norm[:, :-1, 1:]) ** 2
            )
        )
        """
        ltv = torch.sum(
        torch.abs(patch_norm[:, :-1, :-1] - patch_norm[:, 1:, :-1])
        + torch.abs(patch_norm[:, :-1, :-1] - patch_norm[:, :-1, 1:])
        )
        # "Denormalizing" the ltv to match the scale of the original patch

        return ltv

    def _loss(self, images: "torch.Tensor", target: "torch.Tensor", mask: Optional["torch.Tensor"]) -> "torch.Tensor":
        if isinstance(target, torch.Tensor):
            predictions, target = self._predictions(images, mask, target)
            if self.use_logits:
                loss = torch.nn.functional.cross_entropy(
                    input=predictions, target=torch.argmax(target, dim=1), reduction="mean"
                )
            else:
                loss = torch.nn.functional.nll_loss(
                    input=predictions, target=torch.argmax(target, dim=1), reduction="mean"
                )
        else:
            patched_input = self._random_overlay(images, self._patch, mask=mask)
            patched_input = torch.clamp(
                patched_input,
                min=self.estimator.clip_values[0],
                max=self.estimator.clip_values[1],
            )
            patched_input = patched_input.to(self.estimator.device)
            loss = self.estimator.compute_loss(x=patched_input, y=target)

        # Calculate the non-printability score and add it to the loss
        #lnps = self._non_printability_score(self._patch)
        #loss += 0.003*lnps
        # Calculate the total variation loss and add it to the loss
        ltv = self._total_variation_loss(self._patch.clone())
        ltv =0.00005*ltv*self.batch_size
        print(f"ltv: {ltv}")
        loss += ltv
        loss_numerical_value = loss.item()
        print(f"loss: {loss_numerical_value}")
        ltv_percentage = (ltv/loss_numerical_value)*100
        #print percentage of total loss that is due to ltv
        print(f"ltv percentage: {ltv_percentage}%")
        if (not self.targeted and self._optimizer_string != "pgd") or self.targeted and self._optimizer_string == "pgd":
            loss = -loss
        return loss


    

