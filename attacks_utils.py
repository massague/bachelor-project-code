import numpy as np
import torch
from typing import Optional
from art.estimators.object_detection.pytorch_yolo import PyTorchYolo
from art.attacks.evasion import AdversarialPatchPyTorch
import yolov5
from yolov5.utils.loss import ComputeLoss
import cv2
import matplotlib.pyplot as plt
class AdversarialPatchPyTorchModified(AdversarialPatchPyTorch): #so we can monitor the evolution of the loss
    def __init__(self, *args,  **kwargs):
        self.total_batches = kwargs.pop('total_batches', None) # Extract total_batches from kwargs
        super().__init__(*args, **kwargs)
        self.loss_history = []
        self.grad_magnitude_history = []
        self.iter_average_loss_history = []
        self.iter_average_grad_magnitude_history = []
        self.batch_couter = 0

    def _train_step(
            self, images: "torch.Tensor", target: "torch.Tensor", mask: Optional["torch.Tensor"] = None
        ) -> "torch.Tensor":
        images = images.to(self.estimator.device) 
        self.estimator.model.zero_grad()
        loss = self._loss(images, target, mask)
        loss.backward(retain_graph=True)
        

        if self._optimizer_string == "pgd":
            if self._patch.grad is not None:
                gradients = self._patch.grad.sign() * self.learning_rate
            else:
                raise ValueError("Gradient term in PyTorch model is `None`.")

            with torch.no_grad():
                self._patch[:] = torch.clamp(
                    self._patch + gradients, min=self.estimator.clip_values[0], max=self.estimator.clip_values[1]
                )
        else:
            self._optimizer.step()

            with torch.no_grad():
                self._patch[:] = torch.clamp(
                    self._patch, min=self.estimator.clip_values[0], max=self.estimator.clip_values[1]
                )

        self.loss_history.append(loss.item()) #store loss value
        grad_magnitude = torch.norm(self._patch.grad).item()
        self.grad_magnitude_history.append(grad_magnitude)
        self.batch_couter += 1
        
        # Reset variables after all batches for an iteration are processed
        if self.batch_couter == self.total_batches:
            avg_loss = sum(self.loss_history)/self.batch_couter
            avg_grad_magnitude = sum(self.grad_magnitude_history)/self.batch_couter
            self.iter_average_loss_history.append(avg_loss)
            self.iter_average_grad_magnitude_history.append(avg_grad_magnitude)
            self.loss_history = []
            self.grad_magnitude_history = []
            self.batch_couter = 0


        return loss

    def _loss(self, images: "torch.Tensor", target: "torch.Tensor", mask: Optional["torch.Tensor"]) -> "torch.Tensor":
        if isinstance(target, torch.Tensor):
            predictions, target = self._predictions(images, mask, target)
            if self.use_logits:
                loss = torch.nn.functional.cross_entropy(
                    input=predictions, target=torch.argmax(target, dim=1), reduction="mean"
                )
            else:
                loss = torch.nn.functional.nll_loss(
                    input=predictions, target=torch.argmax(target, dim=1), reduction="mean"
                )
        else:
            patched_input = self._random_overlay(images, self._patch, mask=mask)
            patched_input = torch.clamp(
                patched_input,
                min=self.estimator.clip_values[0],
                max=self.estimator.clip_values[1],
            )
            patched_input = patched_input.to(self.estimator.device)
            loss = self.estimator.compute_loss(x=patched_input, y=target)

        # Calculate the non-printability score and add it to the loss
        #lnps = self._non_printability_score(self._patch)
        #loss += 0.003*lnps
        # Calculate the total variation loss and add it to the loss
        #ltv = self._total_variation_loss(self._patch.clone())
        #ltv =0.00005*ltv*self.batch_size
        #ltv=ltv/4 #cause 200x200 pixels
        #print(f"ltv: {ltv}")
        #loss += ltv
        #print(f"loss: {loss}")
        if (not self.targeted and self._optimizer_string != "pgd") or self.targeted and self._optimizer_string == "pgd":
            loss = -loss
        return loss

COCO_INSTANCE_CATEGORY_NAMES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
        'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
        'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
        'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
        'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 
        'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 
        'teddy bear', 'hair drier', 'toothbrush']

def nms(boxes, scores, iou_thresh):
    if len(boxes) == 0:
        return []

    x1 = np.array([box[0][0] for box in boxes])
    y1 = np.array([box[0][1] for box in boxes])
    x2 = np.array([box[1][0] for box in boxes])
    y2 = np.array([box[1][1] for box in boxes])

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    sorted_indices = np.argsort(scores)[::-1]

    keep = []
    while len(sorted_indices) > 0:
        i = sorted_indices[0]
        keep.append(i)

        xx1 = np.maximum(x1[i], x1[sorted_indices[1:]])
        yy1 = np.maximum(y1[i], y1[sorted_indices[1:]])
        xx2 = np.minimum(x2[i], x2[sorted_indices[1:]])
        yy2 = np.minimum(y2[i], y2[sorted_indices[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        iou = inter / (areas[i] + areas[sorted_indices[1:]] - inter)

        sorted_indices = sorted_indices[np.where(iou <= iou_thresh)[0] + 1]

    return keep

def extract_predictions(predictions_, conf_thresh=0.7, iou_thresh=0.5):
    # Get the predicted class
    predictions_class = [COCO_INSTANCE_CATEGORY_NAMES[i] for i in list(predictions_["labels"])]
    if len(predictions_class) < 1:
        return [], [], []
    
    # Get the predicted bounding boxes
    predictions_boxes = [[(i[0], i[1]), (i[2], i[3])] for i in list(predictions_["boxes"])]

    # Get the predicted prediction score
    predictions_score = list(predictions_["scores"])

    # Get a list of indices with scores greater than the threshold
    predictions_t = [predictions_score.index(x) for x in predictions_score if x > conf_thresh]
    if len(predictions_t) < 1:
        return [], [], []

    # Filter predictions based on confidence threshold
    predictions_boxes = [predictions_boxes[i] for i in predictions_t]
    predictions_class = [predictions_class[i] for i in predictions_t]
    predictions_scores = [predictions_score[i] for i in predictions_t]

    # Apply Non-Maximum Suppression
    selected_indices = nms(predictions_boxes, predictions_scores, iou_thresh)

    # Select only the predicted boxes, classes, and scores that are selected after NMS
    predictions_boxes = [predictions_boxes[i] for i in selected_indices]
    predictions_class = [predictions_class[i] for i in selected_indices]
    predictions_scores = [predictions_scores[i] for i in selected_indices]

    return predictions_class, predictions_boxes, predictions_scores

def load_model():
    class Yolo(torch.nn.Module):
        def __init__(self, model):
            super().__init__()
            self.model = model
            self.model.hyp = {'box': 0.05,
                                'obj': 1.0,
                                'cls': 0.5,
                                'anchor_t': 4.0,
                                'cls_pw': 1.0,
                                'obj_pw': 1.0,
                                'fl_gamma': 0.0
                                }
            self.compute_loss = ComputeLoss(self.model.model.model)

        def forward(self, x, targets=None):
            if self.training:
                outputs = self.model.model.model(x)
                loss, loss_items = self.compute_loss(outputs, targets)
                loss_components_dict = {"loss_total": loss}
                return loss_components_dict
            else:
                return self.model(x)

    # Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    # Updated path (server)
    #model = yolov5.load('/home/massague/Bachelor_Project_code/yolov5s.pt')
    model = yolov5.load('yolov5s.pt')

        
    model = Yolo(model)
    model.to(device)
    return  PyTorchYolo(model=model,
                        device_type= 'cuda' if torch.cuda.is_available() else 'cpu',
                        input_shape=(3, 640, 640),
                        clip_values=(0, 255), 
                        attack_losses=("loss_total",))

def save_image_with_boxes(img, boxes, pred_cls, save_path):
    
    text_size = 1
    text_th = 2
    rect_th = 1
    """
    title_height = 70
    title_bg_color = (255, 255, 255)
    title_text_color = (0, 0, 0)
    
    person_boxes = [box for box, cls in zip(boxes, pred_cls) if cls == "person"]

    person_area_sum = sum([(box[1][0] - box[0][0]) * (box[1][1] - box[0][1]) for box in person_boxes])
    img_area = img.shape[0] * img.shape[1]
    person_area_ratio = person_area_sum / img_area

    img[:title_height, :, :] = title_bg_color

    # Draw the number of person objects detected
    num_person_text = f"Number of person objects detected: {len(person_boxes)}"
    cv2.putText(img, num_person_text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, text_size, title_text_color, thickness=text_th)

    # Draw the person area ratio
    area_ratio_text = f"Person area ratio: {person_area_ratio:.2f}"
    cv2.putText(img, area_ratio_text, (10, 60), cv2.FONT_HERSHEY_SIMPLEX, text_size, title_text_color, thickness=text_th)
    """
    for i in range(len(boxes)):
        cv2.rectangle(img, (int(boxes[i][0][0]), int(boxes[i][0][1])), (int(boxes[i][1][0]), int(boxes[i][1][1])),
                      color=(0, 255, 0), thickness=rect_th)
        # Write the prediction class
        cv2.putText(img, pred_cls[i], (int(boxes[i][0][0]), int(boxes[i][0][1])), cv2.FONT_HERSHEY_SIMPLEX, text_size,
                    (0, 255, 0), thickness=text_th)

    img_bgr = cv2.cvtColor(img.astype(np.uint8), cv2.COLOR_RGB2BGR)  # Convert from RGB to BGR
    cv2.imwrite(save_path, img_bgr)

def plots (ap, x, detector, name="no_name"):
    img = x[0].transpose(1,2,0).astype(np.uint8)

    plt.plot(ap.iter_average_loss_history)
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.title('Patch Loss Decrease')
    #plt.savefig(f'/home/massague/Bachelor_Project_code/Plots/{name}_loss_evolution.jpg', format='jpg', bbox_inches='tight')
    plt.savefig(f'Plots/{name}loss_evolution.jpg', format='jpg', bbox_inches='tight')

    plt.figure()
    plt.plot(ap.iter_average_grad_magnitude_history)
    plt.xlabel('Iteration')
    plt.ylabel('Gradient Magnitude')
    plt.title('Patch Gradient Magnitude')
    #plt.savefig(f'/home/massague/Bachelor_Project_code/Plots/{name}_gradient_magnitude_evolution.jpg', format='jpg', bbox_inches='tight')
    plt.savefig(f'Plots/{name}gradient_magnitude_evolution.jpg', format='jpg', bbox_inches='tight')

    patched_image = ap.apply_patch(x, scale=0.2)
    patched_image_hwc = patched_image[0].transpose(1,2,0).astype(np.uint8)
    patched_image_bgr = cv2.cvtColor(patched_image_hwc, cv2.COLOR_RGB2BGR)
    #cv2.imwrite(f'/home/massague/Bachelor_Project_code/Plots/{name}_patched_image.jpg', patched_image_bgr)
    cv2.imwrite(f'Plots/{name}patched_image.jpg', patched_image_bgr)

    threshold = 0.7
    dets = detector.predict(x)
    preds = extract_predictions(dets[0], threshold)
    #save_image_with_boxes(img=img, boxes=preds[1], pred_cls=preds[0], save_path=f"/home/massague/Bachelor_Project_code/Plots/{name}_Predictions_on_image_without_patch.png")
    save_image_with_boxes(img=img, boxes=preds[1], pred_cls=preds[0], save_path=f"Plots/{name}Predictions_on_image_without_patch.png")


    dets = detector.predict(patched_image)
    preds = extract_predictions(dets[0], threshold)
    #save_image_with_boxes(img=patched_image[0].transpose(1,2,0).copy(), boxes=preds[1], pred_cls=preds[0], save_path=f"/home/massague/Bachelor_Project_code/Plots/{name}_Predictions_on_image_with_patch.png")
    save_image_with_boxes(img=patched_image[0].transpose(1,2,0).copy(), boxes=preds[1], pred_cls=preds[0], save_path=f"Plots/{name}Predictions_on_image_with_patch.png")

    from PIL import Image
    patch_to_save = ap._patch.detach().cpu().numpy()  # Converts the tensor to a numpy array

    patch_to_save = np.transpose(patch_to_save, (1, 2, 0))  # Reshapes from (C, H, W) to (H, W, C)

    patch_to_save = (patch_to_save * 255 * 255).astype(np.uint8)  # Converts from float in [0, 1] to uint8 in [0, 255]

    img = Image.fromarray(patch_to_save, 'RGB')
    img.save(f'Plots/{name}.jpg')
    
    

