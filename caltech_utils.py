import json
import glob
import cv2
import numpy as np
import random
from dataset_utils import resize_image, pad_annotations

def load_caltech(dataset_path= "caltech_converted_data", target_size=(640, 640), subset='train', num_samples_per_set=215):
    if subset not in ['train', 'test']:
        raise ValueError("subset must be 'train' or 'test'")
        
    if subset == 'train':
        set_range = range(6)
    else:
        set_range = range(6, 11)

    # Load annotation file paths
    annotation_paths = []
    for i in set_range:
        #set_annotation_paths = sorted(glob.glob(f"/home/massague/Bachelor_Project_code/caltech_converted_data/set{i:02}/*/annotations/*.json", recursive=True))
        set_annotation_paths = sorted(glob.glob(f"{dataset_path}/set{i:02}/*/annotations/*.json", recursive=True))
        if len(set_annotation_paths) > num_samples_per_set:
            set_annotation_paths = random.sample(set_annotation_paths, num_samples_per_set)
        annotation_paths.extend(set_annotation_paths)

    # Load and preprocess images
    images = []
    scales_x = []
    scales_y = []
    for ann_path in annotation_paths:
        img_path = ann_path.replace('annotations', 'images').replace('.json', '.jpg')
        img = cv2.imread(img_path) #Read image
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) #Convert the image from BGR to RGB
        
        img, scale_x, scale_y = resize_image(img, target_size) # Resize and pad the image and get the scaling factor
        scales_x.append(scale_x) # Save the scaling factor x
        scales_y.append(scale_y) # Save the scaling factor y
        
        img = img.transpose((2, 0, 1)) # Reshape to CHW format
        images.append(img)

    if not images:
        raise ValueError("No images found in the specified dataset path and subset.")

    # Stack images into a single numpy array
    images = np.stack(images, axis=0).astype(np.float32)

    # Parse the JSON annotations
    annotations = parse_caltech_json_annotations(annotation_paths, scales_x, scales_y)

    return images, annotations


def parse_caltech_json_annotations(annotation_paths, scales_x, scales_y):
    annotations = []

    for i, ann_path in enumerate(annotation_paths):
        with open(ann_path, 'r') as f:
            data = json.load(f)

        scale_x = scales_x[i]
        scale_y = scales_y[i]
        boxes = []
        labels = []
        scores = []

        for obj in data:
            if 'pos' in obj:
                bbox_coords = [
                    float(obj['pos'][0]) * scale_x,
                    float(obj['pos'][1]) * scale_y,
                    float(obj['pos'][2]) * scale_x,
                    float(obj['pos'][3]) * scale_y
                ]

                boxes.append(bbox_coords)
                labels.append(0 if obj['lbl'] == 'person' else 1) # Assuming 'person' class has a label of 0 and 'people' class has a label of 1
                scores.append(1.0)  # Ground truth confidence score

        boxes = np.array(boxes)
        labels = np.array(labels)
        scores = np.array(scores)

        annotation = {
                "boxes": boxes,
                "labels": labels,
                "scores": scores,
        }

        annotations.append(annotation)

    return annotations
"""
def save_image_with_boxes(img, boxes, pred_cls, save_path):
    text_size = 1
    text_th = 2
    rect_th = 1
    for i in range(len(boxes)):
        cv2.rectangle(img, (int(boxes[i][0][0]), int(boxes[i][0][1])), (int(boxes[i][1][0]), int(boxes[i][1][1])),
                      color=(0, 255, 0), thickness=rect_th)
        # Write the prediction class
        cv2.putText(img, pred_cls[i], (int(boxes[i][0][0]), int(boxes[i][0][1])), cv2.FONT_HERSHEY_SIMPLEX, text_size,
                    (0, 255, 0), thickness=text_th)

    img_bgr = cv2.cvtColor(img.astype(np.uint8), cv2.COLOR_RGB2BGR)  # Convert from RGB to BGR
    cv2.imwrite(save_path, img_bgr)

def get_class_name(label):
    if label == 0:
        return "person"
    else:
        return "unknown"  # Add a default value or handle other labels if necessary

def main():
    images_x, ground_truths = load_caltech("caltech_converted_data", subset="train", num_samples_per_set=1)
    for i, img in enumerate(images_x):
        img = img[np.newaxis]
        ground_truth = ground_truths[i]
        gt_boxes, gt_classes = zip(*[(box, cls) for box, cls in zip(ground_truth["boxes"], ground_truth["labels"]) if cls == 0])
        gt_boxes = list(gt_boxes)
        gt_classes = list(gt_classes)
        gt_boxes = [((box[0], box[1]), (box[0] + box[2], box[1] + box[3])) for box in gt_boxes]  # Update the format of ground truth boxes
        class_names = [get_class_name(label) for label in gt_classes]
        print("gt_boxes:", gt_boxes)
        print("gt_classes:", gt_classes)
        print("----------")
        save_image_with_boxes(img=img[0].transpose(1,2,0).copy(), boxes=gt_boxes, pred_cls=class_names, save_path=f"evaluation_plots/{i}.png")

if __name__ == "__main__":
    main() 
"""