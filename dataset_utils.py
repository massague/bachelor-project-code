import cv2
import numpy as np

def pad_annotations(annotations):
    max_boxes = max([len(ann["boxes"]) for ann in annotations])

    padded_annotations = []
    for ann in annotations:
        num_boxes = len(ann["boxes"])
        padding = max_boxes - num_boxes

        padded_boxes = np.pad(ann["boxes"], ((0, padding), (0, 0)), mode="constant", constant_values=-1)
        padded_labels = np.pad(ann["labels"], (0, padding), mode="constant", constant_values=-1)
        padded_scores = np.pad(ann["scores"], (0, padding), mode="constant", constant_values=-1)

        padded_annotation = {
            "boxes": padded_boxes,
            "labels": padded_labels,
            "scores": padded_scores,
        }
        padded_annotations.append(padded_annotation)

    return padded_annotations

def resize_image(img, target_size):

    # Calculate the aspect ratio and the new dimensions
    height, width = img.shape[:2]
    scale_y = target_size[0] / height
    scale_x = target_size[1] / width
    # Resize the image
    img = cv2.resize(img, (640, 640), interpolation=cv2.INTER_LINEAR)


    return img, scale_x, scale_y