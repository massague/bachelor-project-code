# Adversarial Patch Generation for Pedestrian Detection

This repository contains the code for my bachelor project on generating adversarial patches using the AdversarialPatchPyTorch class from the Adversarial Robustness Toolbox (ART). The project focuses on generating patches for the class person of the YOLOv5 object detection algorithm. You can find the project report in [here](https://gitlab.epfl.ch/massague/bachelor-project-code/-/blob/main/Bachelor_Project_Report_Guillem_Massague.pdf)

## Dataset

- **INRIA person dataset**: The INRIA person dataset is a widely used dataset for person detection. It contains images and crops of persons captured from various scenes.

- **Caltech Pedestrian Dataset**: The Caltech Pedestrian Dataset is another popular dataset for pedestrian detection. It consists of videos collected from a moving vehicle and contains a large number of pedestrian instances.

## Usage

To use this repository, follow these steps:

1. Clone the repository to your local machine.
2. Install the necessary dependencies mentioned in the `requirements.txt` file.
3. Download the INRIA person dataset and the Caltech Pedestrian Dataset.
4. Preprocess the datasets accordingly.
5. ...

## Acknowledgments

This project builds upon the Adversarial Robustness Toolbox (ART) developed by IBM Research. I would like to acknowledge their contribution to the field of adversarial machine learning.
Lastly, to all the pedestrians in the INRIA and Caltech datasets who've walked, run, hopped, and cartwheeled their way for this project - your contributions have not gone unnoticed. Although I can’t personally thank each person, know that each one of you has been a stepping stone in my journey. We truly have shared a walk to remember.

## License

This project is licensed under the MIT License.
