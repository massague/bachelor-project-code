from attacks_utils import plots, load_model, AdversarialPatchPyTorchModified
from caltech_utils import load_caltech
from inria_utils import load_inria
from evaluation_metrics import evaluate_patch
import pickle
import numpy as np

#  Attack settings
rotation_max = 10
scale_min = 0.4
scale_max = 1.0
learning_rate = 1
batch_size = 15
max_iter = 18
patch_shape = (3, 100, 100)
optimizer = 'Adam'

def save_ap_state(ap, filename):
    state = {
        'patch': ap._patch,
        'patch_grad': ap._patch.grad,  # Save the gradients
        'loss_history': ap.loss_history,
        'grad_magnitude_history': ap.grad_magnitude_history,
        'max_iter': ap.max_iter,
        'optimizer_state_dict': ap._optimizer.state_dict(),
    }
    with open(filename, 'wb') as file:
        pickle.dump(state, file)

def main():
    print("INRIA + CALTECH ATTACK")
    detector = load_model()

    # Load INRIA dataset
    x_inria, _ = load_inria("INRIA_dataset", subset="train", num_samples=608)

    # Load Caltech dataset
    x_caltech, _ = load_caltech("caltech_converted_data", subset="train", num_samples_per_set=215)
    # Concatenate the two datasets
    x = np.concatenate((x_inria, x_caltech), axis=0)
    target = detector.predict(x)
    print(f'TOTAL NUMBER OF IMAGES: ', len(x))
    total_batches = len(x) // batch_size + (len(x) % batch_size != 0)
    print("Total batches: ", total_batches)
    ap = AdversarialPatchPyTorchModified(
        estimator=detector,
        rotation_max=rotation_max,
        scale_min=scale_min,
        scale_max=scale_max,
        learning_rate=learning_rate,
        batch_size=batch_size,
        max_iter=max_iter,
        patch_shape=patch_shape,
        patch_type='square',
        verbose=True,
        optimizer=optimizer,
        total_batches=total_batches)
    
    ap.generate(x=x, y=target)
    print("Done generating the patch!")
    plots(ap, x, detector, name="18iter_inria_caltech")
    print("Done generating the plots!")
    # Save the state of the AdversarialPatchPyTorchModified object
    save_ap_state(ap, '/home/massague/ap_state_18iter_inria_caltech.pkl')
    print("Done saving the state, starting evaluation!")
    evaluate_patch(ap, detector, "caltech")
    evaluate_patch(ap, detector=detector, dataset="inria")
    print("Done evaluating the patch!")
if __name__ == "__main__":
    main()