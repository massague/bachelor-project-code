import os
import torch
import numpy as np
from art.estimators.object_detection.pytorch_yolo import PyTorchYolo
import cv2
import yolov5
from yolov5.utils.loss import ComputeLoss
from concurrent.futures import ProcessPoolExecutor
import glob
import json
import random

def dataset_cleaning(dataset_path):
    deleted_images = 0
    deleted_annotations = 0

    set_paths = glob.glob(os.path.join(dataset_path, "set00"))

    for set_path in set_paths:
        video_paths = glob.glob(os.path.join(set_path, "V*"))

        for video_path in video_paths:
            annotation_paths = glob.glob(os.path.join(video_path, "annotations", "*.json"))

            for ann_path in annotation_paths:
                with open(ann_path, 'r') as f:
                    objs = json.load(f)

                # Check if there's at least one person or people in the annotations
                has_target_object = False
                for obj in objs:
                    if obj['lbl'] in ['person', 'people']:
                        has_target_object = True
                        break

                # If no target object is found, delete the image and annotation files
                if not has_target_object:
                    # Construct the path to the corresponding image file for the given annotation file
                    img_path = os.path.join(video_path, 'images', os.path.basename(ann_path)[:-5] + '.jpg')
                    
                    if os.path.exists(img_path):
                        os.remove(img_path)
                        deleted_images += 1

                    if os.path.exists(ann_path):
                        os.remove(ann_path)
                        deleted_annotations += 1

    assert deleted_annotations == deleted_images, "Number of deleted images and annotations should be equal."
    return deleted_images, deleted_annotations



def random_sample_dataset(base_path, max_images_per_set=500):
    deleted_images = 0
    remaining_images = 0

    sets = sorted(glob.glob(os.path.join(base_path, "set*")))
    
    for set_path in sets:
        video_paths = sorted(glob.glob(os.path.join(set_path, "V*")))
        
        for video_path in video_paths:
            image_paths = sorted(glob.glob(os.path.join(video_path, 'images', '*.jpg')))
            annotation_paths = sorted(glob.glob(os.path.join(video_path, 'annotations', '*.json')))

            if len(image_paths) > max_images_per_set:
                selected_indices = random.sample(range(len(image_paths)), max_images_per_set)

                selected_image_paths = [image_paths[i] for i in selected_indices]
                selected_annotation_paths = [annotation_paths[i] for i in selected_indices]

                # Remove unselected images and annotations
                for image_path, annotation_path in zip(image_paths, annotation_paths):
                    if image_path not in selected_image_paths:
                        os.remove(image_path)
                        deleted_images += 1

                    if annotation_path not in selected_annotation_paths:
                        os.remove(annotation_path)

                remaining_images += max_images_per_set

            else:
                remaining_images += len(image_paths)

    return deleted_images, remaining_images

COCO_INSTANCE_CATEGORY_NAMES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
        'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
        'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
        'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
        'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 
        'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 
        'teddy bear', 'hair drier', 'toothbrush']

def nms(boxes, scores, iou_thresh):
    if len(boxes) == 0:
        return []

    x1 = np.array([box[0][0] for box in boxes])
    y1 = np.array([box[0][1] for box in boxes])
    x2 = np.array([box[1][0] for box in boxes])
    y2 = np.array([box[1][1] for box in boxes])

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    sorted_indices = np.argsort(scores)[::-1]

    keep = []
    while len(sorted_indices) > 0:
        i = sorted_indices[0]
        keep.append(i)

        xx1 = np.maximum(x1[i], x1[sorted_indices[1:]])
        yy1 = np.maximum(y1[i], y1[sorted_indices[1:]])
        xx2 = np.minimum(x2[i], x2[sorted_indices[1:]])
        yy2 = np.minimum(y2[i], y2[sorted_indices[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        iou = inter / (areas[i] + areas[sorted_indices[1:]] - inter)

        sorted_indices = sorted_indices[np.where(iou <= iou_thresh)[0] + 1]

    return keep


def extract_predictions(predictions_, conf_thresh, iou_thresh=0.6):
    # Get the predicted class
    predictions_class = [COCO_INSTANCE_CATEGORY_NAMES[i] for i in list(predictions_["labels"])]
    if len(predictions_class) < 1:
        return [], [], []
    
    # Get the predicted bounding boxes
    predictions_boxes = [[(i[0], i[1]), (i[2], i[3])] for i in list(predictions_["boxes"])]

    # Get the predicted prediction score
    predictions_score = list(predictions_["scores"])

    # Get a list of indices with scores greater than the threshold
    predictions_t = [predictions_score.index(x) for x in predictions_score if x > conf_thresh]
    if len(predictions_t) < 1:
        return [], [], []

    # Filter predictions based on confidence threshold
    predictions_boxes = [predictions_boxes[i] for i in predictions_t]
    predictions_class = [predictions_class[i] for i in predictions_t]
    predictions_scores = [predictions_score[i] for i in predictions_t]

    # Apply Non-Maximum Suppression
    selected_indices = nms(predictions_boxes, predictions_scores, iou_thresh)

    # Select only the predicted boxes, classes, and scores that are selected after NMS
    predictions_boxes = [predictions_boxes[i] for i in selected_indices]
    predictions_class = [predictions_class[i] for i in selected_indices]
    predictions_scores = [predictions_scores[i] for i in selected_indices]

    return predictions_class, predictions_boxes, predictions_scores


def load_model():
    class Yolo(torch.nn.Module):
        def __init__(self, model):
            super().__init__()
            self.model = model
            self.model.hyp = {'box': 0.05,
                                'obj': 1.0,
                                'cls': 0.5,
                                'anchor_t': 4.0,
                                'cls_pw': 1.0,
                                'obj_pw': 1.0,
                                'fl_gamma': 0.0
                                }
            self.compute_loss = ComputeLoss(self.model.model.model)

        def forward(self, x, targets=None):
            if self.training:
                outputs = self.model.model.model(x)
                loss, loss_items = self.compute_loss(outputs, targets)
                loss_components_dict = {"loss_total": loss}
                return loss_components_dict
            else:
                return self.model(x)

    model = yolov5.load('/Users/guillemmassague/Desktop/Bachelor Project code/yolov5s.pt')
        
    #model = Yolo(model)

    return  PyTorchYolo(model=model,
                        device_type='cpu',
                        input_shape=(3, 640, 640),
                        clip_values=(0, 255), 
                        attack_losses=("loss_total",))

"""
def keep_just_where_yolo_detects(dataset_path, detector, set_number=0):
    def process_v_folder(set_path, v_folder, detector):
        v_path = os.path.join(set_path, v_folder)
        deleted_images_count = 0

        if os.path.isdir(v_path) and v_folder.startswith("V"):
            print(f"Processing {v_folder}")

            annotation_path = os.path.join(v_path, "annotations")
            images_path = os.path.join(v_path, "images")


            annotation_files = sorted(os.listdir(annotation_path))

            for i, annotation_file in enumerate(annotation_files):
                if annotation_file.endswith(".json"):
                    img_name = annotation_file.replace(".json", ".jpg")
                    img_path = os.path.join(images_path, img_name)

                    with open(os.path.join(annotation_path, annotation_file), "r") as f:
                        img = cv2.imread(img_path)
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert the image from BGR to RGB
                        img = cv2.resize(img, (640, 640))  # Resize the image

                        img_reshape = img.transpose((2, 0, 1))
                        image = np.stack([img_reshape], axis=0).astype(np.float32)
                        threshold = 0.75

                        detections = detector.predict(image)
                        person_detections = [det for det in extract_predictions(detections[0], threshold)[0] if det == "person"]

                    if not person_detections:
                        os.remove(img_path)
                        os.remove(os.path.join(annotation_path, annotation_file))  # Fix the file removal line
                        deleted_images_count += 1

            print("\nFinished processing", v_folder)
            return deleted_images_count
        return 0
    set_folder = f"set{set_number:02d}"
    set_path = os.path.join(dataset_path, set_folder)

    num_v_folders = len([name for name in os.listdir(set_path) if os.path.isdir(os.path.join(set_path, name)) and name.startswith("V")])

    with ProcessPoolExecutor() as executor:
        results = executor.map(process_v_folder, [set_path]*num_v_folders, sorted(os.listdir(set_path)), [detector]*num_v_folders)

    total_deleted_images = sum(results)
    print(f"\nTotal deleted images in set {set_number:02d}: {total_deleted_images}\n")
"""

def process_v_folder(set_path, v_folder, detector):
        v_path = os.path.join(set_path, v_folder)
        deleted_images_count = 0

        if os.path.isdir(v_path) and v_folder.startswith("V01"):
            print(f"Processing {v_folder}")

            annotation_path = os.path.join(v_path, "annotations")
            images_path = os.path.join(v_path, "images")

            annotation_files = sorted(os.listdir(annotation_path))

            for i, annotation_file in enumerate(annotation_files):
                if annotation_file.endswith(".json"):
                    img_name = annotation_file.replace(".json", ".jpg")
                    img_path = os.path.join(images_path, img_name)

                    with open(os.path.join(annotation_path, annotation_file), "r") as f:
                        img = cv2.imread(img_path)
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert the image from BGR to RGB
                        img = cv2.resize(img, (640, 640))  # Resize the image

                        img_reshape = img.transpose((2, 0, 1))
                        image = np.stack([img_reshape], axis=0).astype(np.float32)
                        threshold = 0.7

                        detections = detector.predict(image)
                        person_boxes = [box for box, det in zip(extract_predictions(detections[0], threshold)[1], extract_predictions(detections[0], threshold)[0]) if det == "person" and len(box) > 0 and len(box[0]) > 1]

                        person_area_sum = sum([(box[1][0] - box[0][0]) * (box[1][1] - box[0][1]) for box in person_boxes])

                        img_area = img.shape[0] * img.shape[1]
                        #assertion image area is 640x640
                        assert img_area == 640 * 640
                        person_area_ratio = 0.1
                    if person_area_sum < person_area_ratio * img_area:
                        os.remove(img_path)
                        os.remove(os.path.join(annotation_path, annotation_file))  # Fix the file removal line
                        deleted_images_count += 1
                        
            print("\nFinished processing", v_folder)
            return deleted_images_count
        return 0

def keep_just_where_yolo_detects_advanced(dataset_path, detector, set_number=0):
    set_folder = f"set{set_number:02d}"
    set_path = os.path.join(dataset_path, set_folder)

    num_v_folders = len([name for name in os.listdir(set_path) if os.path.isdir(os.path.join(set_path, name)) and name.startswith("V")])
    print("Starting processing with ProcessPoolExecutor")

    with ProcessPoolExecutor() as executor:
        results = executor.map(process_v_folder, [set_path]*num_v_folders, sorted(os.listdir(set_path)), [detector]*num_v_folders)

    total_deleted_images = sum(results)
    print(f"\nTotal deleted images in set {set_number:02d}: {total_deleted_images}\n")



def main():
    dataset_path = "/Users/guillemmassague/Desktop/Bachelor Project code/caltech_converted_data"
    print(f"Dataset path: {dataset_path}")
    model = load_model()
    keep_just_where_yolo_detects_advanced(dataset_path, model, set_number=0)


    #for set_number in range(1, 11):
        #keep_just_where_yolo_detects_advanced(dataset_path, model, set_number=set_number)
    
    # Call the dataset_cleaning function
    #deleted_images, deleted_annotations = dataset_cleaning('caltech_converted_data')
    #print(f"Removed after keeping just images with \"person\" or \"people\" {deleted_images} images and {deleted_annotations} annotations without people.")

if __name__ == '__main__':
    main()



