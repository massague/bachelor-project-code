import numpy as np
import torch
from inria_utils import load_inria
from dataset_utils import pad_annotations
from caltech_utils import load_caltech
import tqdm
from scipy.optimize import linear_sum_assignment
import copy
from attacks_utils import load_model, extract_predictions, save_image_with_boxes, AdversarialPatchPyTorchModified

def calculate_metrics(img, ground_truth, dataset, conf_threshold=0.7, iou_threshold=0.7, detector=None, image_name="test"):
    # Evaluation of effectiveness of the patch
    if dataset == "inria":
        gt_classes, gt_boxes = ground_truth["labels"], ground_truth["boxes"]
        gt_boxes = [((box[0], box[1]), (box[2], box[3])) for box in gt_boxes]  # Update the format of ground truth boxes
    elif dataset == "caltech":
        gt_boxes, gt_classes = zip(*[(box, cls) for box, cls in zip(ground_truth["boxes"], ground_truth["labels"]) if cls == 0])
        gt_boxes = list(gt_boxes)
        gt_classes = list(gt_classes)
        gt_boxes = [((box[0], box[1]), (box[0] + box[2], box[1] + box[3])) for box in gt_boxes]
    else:
        raise ValueError("Invalid dataset name. Supported datasets: 'inria', 'caltech'")
    
    if detector is None:
        detector = load_model()

    dets = detector.predict(img)
    pred_classes, pred_boxes, pred_scores = extract_predictions(dets[0], conf_thresh=conf_threshold, iou_thresh=iou_threshold)
    num_persons_detected = pred_classes.count('person')

    #print("gt_boxes:", gt_boxes)
    #print("gt_classes:", gt_classes)
    #print("pred_boxes:", pred_boxes)
    #print("pred_classes:", pred_classes)

    # Initialize counts and IoU list
    tp, fp, fn = 0, 0, 0

    # Calculate IoU matrix
    iou_matrix = np.zeros((len(gt_boxes), len(pred_boxes)))
    for i, gt_box in enumerate(gt_boxes):
        for j, pred_box in enumerate(pred_boxes):
            if gt_classes[i] == 0 and pred_classes[j] == 'person':
                iou_matrix[i, j] = calculate_iou(gt_box, pred_box)

    # Find optimal matching using the Hungarian algorithm
    row_indices, col_indices = linear_sum_assignment(-iou_matrix)

    # Calculate TP, FP, and FN based on the matching and IoU threshold
    for row, col in zip(row_indices, col_indices):
        if gt_classes[row] == 0:
            iou = iou_matrix[row, col]
            if iou_matrix[row, col] >= iou_threshold:
                tp += 1
                #print(f"Matched ground truth box {row} with predicted box {col}, IoU: {iou}")
            else:
                fn += 1
                #print(f"Unmatched ground truth box {row}, IoU: {iou}")
    fp = sum(1 for j, pred_class in enumerate(pred_classes) if pred_class == 'person' and j not in col_indices)
    #save_image_with_boxes(img=img[0].transpose(1,2,0).copy(), boxes=pred_boxes, pred_cls=pred_classes, save_path=f"evaluation_plots/pred_{image_name}.png")
    class_names = [get_class_name(label) for label in gt_classes]
    #save_image_with_boxes(img=img[0].transpose(1,2,0).copy(), boxes=gt_boxes, pred_cls=class_names, save_path=f"evaluation_plots/gt_{image_name}.png")
    #print (f"TP: {tp}, FP: {fp}, FN: {fn}")
    return num_persons_detected, tp, fp, fn

def calculate_iou(boxA, boxB):
    xA = max(boxA[0][0], boxB[0][0])
    yA = max(boxA[0][1], boxB[0][1])
    xB = min(boxA[1][0], boxB[1][0])
    yB = min(boxA[1][1], boxB[1][1])

    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    boxAArea = (boxA[1][0] - boxA[0][0] + 1) * (boxA[1][1] - boxA[0][1] + 1)
    boxBArea = (boxB[1][0] - boxB[0][0] + 1) * (boxB[1][1] - boxB[0][1] + 1)
    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou

def get_class_name(label):
    if label == 0:
        return "person"
    else:
        return "unknown"  # Add a default value or handle other labels if necessary

def evaluate_patch(ap, detector, dataset):

    # Evaluation of effectiveness of the patch
    if dataset == "inria":
        # Load the INRIA test dataset images and annotations
        test_images, ground_truth_test = load_inria(dataset_path="INRIA_dataset", target_size=(640, 640), subset='test')
    elif dataset == "caltech":
        # Load the Caltech test dataset images and annotations
        test_images, ground_truth_test = load_caltech(target_size=(640, 640), subset='test')
    else:
        raise ValueError("Invalid dataset name. Supported datasets: 'inria', 'caltech'")
    
    total_number_of_persons = sum(np.sum(ann["labels"] != -1) for ann in ground_truth_test)

    print(f"Total number of persons in the ground truth test dataset: {total_number_of_persons}")

    def evaluate_scenario(images, _ap=None, scenario_name="test"):
        tp, fp, fn = 0, 0, 0
        iou_list = []
        total_persons_detected = 0

        for i, img in tqdm.tqdm(enumerate(images), total=len(images)):
            img = img[np.newaxis]
            if _ap:
                img = _ap.apply_patch(img, scale=0.2)
            ground_truth = ground_truth_test[i]
            num_persons,true_pos, false_pos, false_neg = calculate_metrics(img, ground_truth, dataset=dataset, detector=detector, image_name=f"{scenario_name}_{i}")
            total_persons_detected += num_persons
            tp += true_pos
            fp += false_pos
            fn += false_neg

        return total_persons_detected, tp, fp, fn, iou_list
    
    # Evaluate without patch
    total_persons_no_patch, tp_no_patch, fp_no_patch, fn_no_patch, iou_list_no_patch = evaluate_scenario(test_images, scenario_name="no_patch")
    
    # Evaluate with random control patch
    ap_control = copy.deepcopy(ap)
    ap_control._patch = torch.rand(*ap_control.patch_shape)
    total_persons_random_patch, tp_random_patch, fp_random_patch, fn_random_patch, iou_list_random_patch = evaluate_scenario(test_images, _ap=ap_control, scenario_name="random_patch")
    # Evaluate with the adversarial patch
    total_persons_adversarial_patch, tp_adversarial_patch, fp_adversarial_patch, fn_adversarial_patch, iou_list_adversarial_patch = evaluate_scenario(test_images, _ap=ap, scenario_name="adversarial_patch")
    def print_metrics(title, total_persons, tp, fp, fn, iou_list):
        precision = tp / (tp + fp) if tp + fp > 0 else 0
        recall = tp / (tp + fn) if tp + fn > 0 else 0
        f1_score = 2 * (precision * recall) / (precision + recall) if precision + recall > 0 else 0
        print(f"{title}")
        print("-------------------------------")
        print(f"Total number of persons detected: {total_persons}")
        print(f"Precision: {precision:.2f}")
        print(f"Recall: {recall:.2f}")
        print(f"F1-score: {f1_score:.2f}")
        print("-------------------------------\n")

    #True Positive (tp): The number of correctly detected persons (i.e., the number of persons in the ground truth that were correctly identified by the model as persons).
    #False Positive (fp): The number of incorrect detections (i.e., the number of objects that the model identified as persons, but they were not persons in the ground truth).
    #False Negative (fn): The number of missed persons (i.e., the number of persons in the ground truth that the model failed to identify).  
    print_metrics("No Patch",total_persons_no_patch, tp_no_patch, fp_no_patch, fn_no_patch, iou_list_no_patch)
    print_metrics("Random Control Patch", total_persons_random_patch, tp_random_patch, fp_random_patch, fn_random_patch, iou_list_random_patch)
    print_metrics("Adversarial Patch", total_persons_adversarial_patch, tp_adversarial_patch, fp_adversarial_patch, fn_adversarial_patch, iou_list_adversarial_patch)
